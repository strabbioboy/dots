call plug#begin('~/.local/share/nvim/plugged')
Plug 'scrooloose/nerdtree'
Plug 'mhinz/vim-startify'
"Plug 'morhetz/gruvbox'
Plug 'vim-airline/vim-airline'
Plug 'vim-airline/vim-airline-themes'
Plug 'ycm-core/YouCompleteMe'
Plug 'junegunn/fzf.vim'
Plug 'chriskempson/base16-vim'
Plug 'chrisbra/Colorizer'
Plug 'camspiers/animate.vim'
Plug 'camspiers/lens.vim'
call plug#end()

let g:airline_theme="base16_default"
let g:airline#extensions#tabline#enabled = 1
colorscheme base16-default-dark
filetype plugin indent on 			" Detect filetype
set expandtab
set tabstop=2
set softtabstop=2
set shiftwidth=2
set termguicolors				" Enable truecolors
set nobackup					" Dont keep backups file (restore to previous version)
set nowritebackup 				" No backups
set undofile					" Keep an undo file (undo changes after closing)
set undodir=~/.local/share/nvim/undodir		" Keep an undo file in a single directory
set cursorline 					" Highlight line
set laststatus=2				" Always show statusline
set mouse+=a					" Yap
set hlsearch 					" Highlight search
set incsearch 					" Search as characters are entered
set ignorecase  				" Case insensitive search
set smartcase 					" Smartsearch
set gdefault 					" Add the g flag to search/replace by default
set backspace=indent,eol,start			" Let you backspace on everithing
set autoindent					" Copy the indentation of the previous line
set clipboard^=unnamedplus 			" Primary clipboard
set diffopt+=vertical 				" Set vertical split by default	
set number relativenumber

map <C-n> :NERDTreeToggle<CR>
map <C-p> :Files<CR>
map <C-b> :Buffers<CR>

" Shortcutting split navigation
map <C-h> <C-w>h
map <C-j> <C-w>j
map <C-k> <C-w>k
map <C-l> <C-w>l
map <C-q> <C-w>q

" Move between buffers
map <C-Left> <Esc>:bprev<CR>
map <C-Right> <Esc>:bnext<CR>
